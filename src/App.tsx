import React from "react";
import { useSelector } from "react-redux";

import Auth from "./components/Auth";
import Counter from "./components/Counter";
import Header from "./components/Header";
import UserProfile from "./components/UserProfile";

interface AuthState {
  auth: {
    isAuthenticated: boolean;
  };
}

function App() {
  const isAuth = useSelector((state: AuthState) => state.auth.isAuthenticated);
  return (
    <>
      <Header />
      {!isAuth && <Auth />}
      {isAuth && <UserProfile />}
      <Counter />
    </>
  );
}

export default App;
