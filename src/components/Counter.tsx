import React, { Component, Dispatch } from "react";
import { connect, useDispatch, useSelector } from "react-redux";

import { counterActions } from "../store/counterSlice";
import classes from "./Counter.module.css";

interface CounterState {
  counter: {
    counter: number;
    showCounter: boolean;
  };
}

interface CounterProps {
  increment: () => void;
  decrement: () => void;
  toggle: () => void;
  counter: number;
  showCounter: boolean;
}

const Counter = () => {
  const dispatch = useDispatch();
  const counter = useSelector((state: CounterState) => state.counter.counter);
  const show = useSelector((state: CounterState) => state.counter.showCounter);

  const incrementHandler = () => {
    dispatch(counterActions.increment());
  };
  const increaseHandler = () => {
    dispatch(counterActions.increase(5));
  };
  const decrementHandler = () => {
    dispatch(counterActions.decrement());
  };
  const toggleCounterHandler = () => {
    dispatch(counterActions.toggleCounter());
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {show && <div className={classes.value}>{counter}</div>}
      <div>
        <button onClick={incrementHandler}>Increment</button>
        <button onClick={increaseHandler}>Increase by 5</button>
        <button onClick={decrementHandler}>Decrement</button>
      </div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;

// class Counter extends Component<CounterProps, CounterState> {
//   incrementHandler() {
//     this.props.increment();
//   }
//   decrementHandler() {
//     this.props.decrement();
//   }
//   toggleCounterHandler() {
//     this.props.toggle();
//   }
//
//   render() {
//     return (
//       <main className={classes.counter}>
//         <h1>Redux Counter</h1>
//         {this.props.showCounter && (
//           <div className={classes.value}>{this.props.counter}</div>
//         )}
//         <div>
//           <button onClick={this.incrementHandler.bind(this)}>Increment</button>
//           <button onClick={this.decrementHandler.bind(this)}>Decrement</button>
//         </div>
//         <button onClick={this.toggleCounterHandler.bind(this)}>
//           Toggle Counter
//         </button>
//       </main>
//     );
//   }
// }
//
// const mapStateToProps = (state: CounterState) => {
//   return {
//     counter: state.counter,
//     showCounter: state.showCounter,
//   };
// };
//
// const mapDispatchToProps = (dispatch: Dispatch<{ type: string }>) => {
//   return {
//     increment: () => dispatch({ type: "counter/incremented" }),
//     decrement: () => dispatch({ type: "counter/decremented" }),
//     toggle: () => dispatch({ type: "counter/toggle" }),
//   };
// };
//
// const enhance = connect(mapStateToProps, mapDispatchToProps);
// const ConnectedCounter = enhance(Counter);
// export default ConnectedCounter;
// // export default connect(mapStateToProps, mapDispatchToProps)(Counter);
