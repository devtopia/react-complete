## Overview

このサンプルはMaximilianさんのUdemyの講座に出たサンプルをTypeScriptで作成しました。
Eslintでエラーが出ないように修正したため、JavaScriptで作成された講座のサンプルとはかなり違いがあります。

Reduxのサンプルを以下の３段階で作成されています。
1. class componentで作成
2. function componentで作成
3. RTK(Redux ToolKit)で作成

## How to set up the app

packageをインストールする。

```bash
yarn install
```

yarnがインストールされていない場合は、先にyarnをインストールする。

```bash
npm install -g yarn
```

devサーバーを起動する。

```bash
yarn start
```

ブラウザで確認する。

http://localhost:3000

![ReactAuth.gif](./public/ReduxAuth.gif)
